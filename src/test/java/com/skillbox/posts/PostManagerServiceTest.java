package com.skillbox.posts;

import com.skillbox.posts.entity.Photo;
import com.skillbox.posts.entity.Post;
import com.skillbox.posts.gateway.UsersGateway;
import com.skillbox.posts.service.PhotoService;
import com.skillbox.posts.service.PostService;
import com.skillbox.posts.service.PostsManagerServiceImpl;
import com.skillbox.posts.service.PostsMangerService;
import com.skillbox.posts.service.S3Service;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = PostsManagerServiceImpl.class)
public class PostManagerServiceTest {

    @Autowired
    private PostsMangerService postsMangerService;
    @MockBean
    private PostService postService;
    @MockBean
    private UsersGateway usersGateway;
    @MockBean
    private PhotoService photoService;
    @MockBean
    private S3Service s3Service;

    @Test
    public void get_all_users_post() {
        //given
        Long userId = 1L;
        Post post1 = new Post();
        post1.setUserId(1L);
        Post post2 = new Post();
        post2.setUserId(1L);
        List<Post> posts = List.of(post1, post2);
        when(postService.findAllPostByUserId(userId)).thenReturn(posts);
        when(usersGateway.existUserById(userId)).thenReturn(true);
        //when
        var result = postsMangerService.getAllUserPosts(userId);
        //then
        assertEquals(posts, result);
        verify(postService, times(1)).findAllPostByUserId(userId);
    }

    @Test
    public void create_post() {
        //given
        Long userId = 1L;
        Post post = new Post();
        post.setUserId(userId);
        Photo photo = new Photo();
        photo.setPost(post);
        photo.setName("Photo");
        photo.setLink("url");
        List<Photo> photos = List.of(photo);
        Post enrichedPostByPhotos = new Post();
        enrichedPostByPhotos.setPhotos(photos);
        enrichedPostByPhotos.setUserId(userId);
        List<MultipartFile> files = new ArrayList<>();

        when(usersGateway.existUserById(userId)).thenReturn(true);
        when(s3Service.saveAndGetPhotos(files)).thenReturn(photos);
        when(postService.savePost(enrichedPostByPhotos)).thenReturn(enrichedPostByPhotos);

        //when
        Post result = postsMangerService.createPost(userId, post, files);

        //then
        assertEquals(enrichedPostByPhotos, result);
        verify(usersGateway, times(1)).existUserById(userId);
        verify(s3Service, times(1)).saveAndGetPhotos(files);
        verify(postService, times(1)).savePost(enrichedPostByPhotos);
    }

    @Test
    public void save_post_for_post() {
        //given
        Long userId = 1L;
        Long postId = 1L;
        Post post = new Post();
        post.setId(postId);
        post.setUserId(userId);
        Photo photo = new Photo();
        photo.setPost(post);
        photo.setName("Photo");
        photo.setLink("url");
        List<Photo> photos = List.of(photo);
        Post enrichedPostByPhotos = new Post();
        enrichedPostByPhotos.setPhotos(photos);
        enrichedPostByPhotos.setUserId(userId);
        List<MultipartFile> files = new ArrayList<>();

        when(usersGateway.existUserById(userId)).thenReturn(true);
        when(s3Service.saveAndGetPhotos(files)).thenReturn(photos);
        when(postService.savePost(enrichedPostByPhotos)).thenReturn(enrichedPostByPhotos);
        when(postService.existPostById(postId)).thenReturn(true);
        when(postService.findPostByPostId(postId)).thenReturn(post);

        //when
        postsMangerService.savePhotosForPost(userId, postId, files);

        //then
        verify(usersGateway, times(1)).existUserById(userId);
        verify(s3Service, times(1)).saveAndGetPhotos(files);
        verify(postService, times(1)).findPostByPostId(postId);
    }

    @Test
    public void delete_photos_from_post() {
        //given
        Long userId = 1L;
        Long postId = 1L;
        Post post = new Post();
        post.setId(postId);
        post.setUserId(userId);
        Photo photo = new Photo();
        photo.setPost(post);
        photo.setName("Photo");
        photo.setLink("url");
        Long photoId = 1L;
        when(usersGateway.existUserById(userId)).thenReturn(true);
        when(postService.existPostById(postId)).thenReturn(true);
        when(photoService.getPhotoById(photoId)).thenReturn(photo);
        doNothing().when(photoService).deletePhoto(photo);
        doNothing().when(s3Service).deletePhoto(photo.getName());

        //when
        postsMangerService.deletePhotoFromPost(userId, postId, photoId);

        //then
        verify(usersGateway, times(1)).existUserById(userId);
        verify(postService, times(1)).existPostById(postId);
        verify(photoService, times(1)).getPhotoById(photoId);
        verify(photoService, times(1)).deletePhoto(photo);
        verify(s3Service, times(1)).deletePhoto(photo.getName());
    }

    @Test
    public void update_post() {
        //given
        Long userId = 1L;
        Long postId = 1L;
        Post post = new Post();
        post.setId(postId);
        post.setUserId(userId);
        Photo photo = new Photo();
        photo.setPost(post);
        photo.setName("Photo");
        photo.setLink("url");
        when(usersGateway.existUserById(userId)).thenReturn(true);
        when(postService.existPostById(postId)).thenReturn(true);
        when(postService.findPostByPostId(postId)).thenReturn(post);
        when(postService.savePost(post)).thenReturn(post);

        //when
        postsMangerService.updatePost(userId, postId, post);

        //then
        verify(usersGateway, times(1)).existUserById(userId);
        verify(postService, times(1)).existPostById(postId);
        verify(postService, times(1)).findPostByPostId(postId);
        verify(postService, times(1)).savePost(post);
    }

    @Test
    public void delete_user_post() {
        //given
        Long userId = 1L;
        Long postId = 1L;
        Post post = new Post();
        post.setId(postId);
        post.setUserId(userId);
        Photo photo = new Photo();
        photo.setPost(post);
        photo.setName("Photo");
        photo.setLink("url");
        post.setPhotos(List.of(photo));
        when(usersGateway.existUserById(userId)).thenReturn(true);
        when(postService.existPostById(postId)).thenReturn(true);
        when(postService.findPostByPostId(postId)).thenReturn(post);
        doNothing().when(postService).deletePost(post);
        doNothing().when(s3Service).deletePhoto(photo.getName());

        //when
        postsMangerService.deleteUserPost(userId, postId);

        //then
        verify(usersGateway, times(1)).existUserById(userId);
        verify(postService, times(1)).existPostById(postId);
        verify(postService, times(1)).findPostByPostId(postId);
        verify(postService, times(1)).deletePost(post);
        verify(s3Service, times(1)).deletePhoto(photo.getName());
    }

}
