package com.skillbox.posts.converter;

import com.skillbox.posts.dto.PostDtoRequest;
import com.skillbox.posts.dto.PostDtoResponse;
import com.skillbox.posts.entity.Post;
import org.mapstruct.Mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = IGNORE)
public interface PostConverter {
    Post postDtoRequestToPost(PostDtoRequest source);

    PostDtoResponse postToPostResponseDto(Post source);
}
