package com.skillbox.posts.exceptions;

public class CommonExistException extends RuntimeException {
    public CommonExistException(String message) {
        super(message, new IllegalAccessError());
    }
}
