package com.skillbox.posts.exceptions;

import lombok.RequiredArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(1)
@ControllerAdvice
@RequiredArgsConstructor
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

    private final ExceptionConverter exConverter;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> exception(Exception ex) {
        return toResponseEntity(exConverter.mapException(ex));
    }

    private ResponseEntity<Object> toResponseEntity(Error error) {
        return new ResponseEntity<>(
                error,
                HttpStatus.valueOf(error.getStatus())
        );
    }
}
