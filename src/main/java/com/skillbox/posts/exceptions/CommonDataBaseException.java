package com.skillbox.posts.exceptions;

public class CommonDataBaseException extends RuntimeException {
    public CommonDataBaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
