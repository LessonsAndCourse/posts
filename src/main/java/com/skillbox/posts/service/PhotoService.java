package com.skillbox.posts.service;

import com.skillbox.posts.entity.Photo;
import com.skillbox.posts.exceptions.CommonDataBaseException;
import com.skillbox.posts.repository.PhotoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
@RequiredArgsConstructor
public class PhotoService {

    private static final String CANT_DELETE_PHOTO = "Can't delete photo";
    private static final String CANT_FIND_PHOTO_BY_ID = "Can't find photo by id";

    private final PhotoRepository photoRepository;

    public void deletePhoto(Photo photo) {
        try {
            photoRepository.delete(photo);
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_DELETE_PHOTO, ex.getCause());
        }
    }

    public Photo getPhotoById(Long photoId) {
        try {
            return photoRepository.findById(photoId)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_FIND_PHOTO_BY_ID, ex.getCause());
        }
    }
}
