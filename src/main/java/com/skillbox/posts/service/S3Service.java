package com.skillbox.posts.service;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.posts.entity.Photo;
import com.skillbox.posts.repository.S3Repository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class S3Service {

    private final S3Repository s3Repository;


    public List<Photo> saveAndGetPhotos(List<MultipartFile> files) {
        return files.stream()
                .map(this::saveFile)
                .map(this::getPhotoWithUrl)
                .collect(Collectors.toList());
    }

    public void deletePhoto(String key) {
        s3Repository.delete(key);
    }

    private String saveFile(MultipartFile file) {
        try {
            var fileUuid = getUuid();
            s3Repository.put(fileUuid, file.getInputStream(), getObjectMetadata(file));
            return fileUuid;
        } catch (IOException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private Photo getPhotoWithUrl(String uuidFile) {
        var photo = new Photo();
        photo.setName(uuidFile);
        photo.setLink(s3Repository.getFileUrl(uuidFile));
        return photo;
    }

    private ObjectMetadata getObjectMetadata(MultipartFile file) {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.getSize());
        metadata.setContentType(MediaType.IMAGE_JPEG_VALUE);
        return metadata;
    }

    private String getUuid() {
        return UUID.randomUUID().toString();
    }
}
