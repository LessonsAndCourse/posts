package com.skillbox.posts.service;

import com.skillbox.posts.entity.Photo;
import com.skillbox.posts.entity.Post;
import com.skillbox.posts.exceptions.CommonExistException;
import com.skillbox.posts.gateway.UsersGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PostsManagerServiceImpl implements PostsMangerService {

    private static final String USER_DOES_NOT_EXIST = "User does not exist";
    private static final String POST_DOES_NOT_EXIST = "Post does not exist";
    private static final String USER_OR_POST_DOES_NOT_EXIST = "User or post does not exist";

    private final PostService postService;
    private final UsersGateway usersGateway;
    private final PhotoService photoService;
    private final S3Service s3Service;

    @Override
    public List<Post> getAllUserPosts(Long userId) {
        if (userExist(userId)) {
            return postService.findAllPostByUserId(userId);
        } else {
            throw new CommonExistException(USER_DOES_NOT_EXIST);
        }
    }

    @Override
    public Post createPost(Long userId, Post post, List<MultipartFile> files) {
        if (userExist(userId)) {
            var photos = s3Service.saveAndGetPhotos(files);
            var enrichedPostByPhotos = enrichPostByPhotos(userId, post, photos);
            return postService.savePost(enrichedPostByPhotos);
        } else {
            throw new CommonExistException(USER_DOES_NOT_EXIST);
        }
    }

    @Override
    public void savePhotosForPost(Long userId, Long postId,  List<MultipartFile> files) {
        if (postExist(postId)) {
            var post = postService.findPostByPostId(postId);
            createPost(userId, post, files);
        } else {
            throw new CommonExistException(POST_DOES_NOT_EXIST);
        }
    }

    @Override
    public void deletePhotoFromPost(Long userId, Long postId, Long photoId) {
        if (userExist(userId) && postExist(postId)) {
            var photo = photoService.getPhotoById(photoId);
            photoService.deletePhoto(photo);
            s3Service.deletePhoto(photo.getName());
        } else {
            throw new CommonExistException(USER_OR_POST_DOES_NOT_EXIST);
        }
    }

    @Override
    public void updatePost(Long userId, Long postId, Post post) {
        if (userExist(userId) && postExist(postId)) {
            var postFromDb = postService.findPostByPostId(postId);
            postFromDb.setTitle(post.getTitle());
            postFromDb.setDescription(post.getDescription());
            postService.savePost(postFromDb);
        } else {
            throw new CommonExistException(USER_OR_POST_DOES_NOT_EXIST);
        }
    }

    @Override
    public void deleteUserPost(Long userId, Long postId) {
        if (userExist(userId) && postExist(postId)) {
            var post = postService.findPostByPostId(postId);
            postService.deletePost(post);
            post.getPhotos().forEach(photo -> s3Service.deletePhoto(photo.getName()));
        } else {
            throw new CommonExistException(USER_OR_POST_DOES_NOT_EXIST);
        }
    }

    private boolean userExist(Long userId) {
        return usersGateway.existUserById(userId);
    }

    private boolean postExist(Long postId) {
        return postService.existPostById(postId);
    }

    private Post enrichPostByPhotos(Long userId, Post post, List<Photo> photos) {
        photos.forEach(photo -> {
            photo.setPost(post);
            post.addPhoto(photo);
        });
        post.setUserId(userId);
        return post;
    }

}
