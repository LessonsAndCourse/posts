package com.skillbox.posts.service;

import com.skillbox.posts.entity.Post;
import com.skillbox.posts.exceptions.CommonDataBaseException;
import com.skillbox.posts.repository.PostRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {

    private static final String CANT_FIND_POSTS = "Can't find post by user's id";
    private static final String CANT_SAVE_POST = "Can't save post";
    private static final String CANT_FIND_POST = "Can't find post by postId";
    private static final String CANT_DELETE_POST = "Can't find post by postId";
    private static final String CANT_CHECK_EXIST_POST = "Can't check exist post by postId";

    private final PostRepository postRepository;

    public List<Post> findAllPostByUserId(Long userId) {
        try {
            return postRepository.findAllByUserId(userId);
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_FIND_POSTS, ex.getCause());
        }
    }

    public Post savePost(Post post) {
        try {
            return postRepository.save(post);
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_SAVE_POST, ex.getCause());
        }
    }

    public Post findPostByPostId(Long postId) {
        try {
            return postRepository.findById(postId)
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_FIND_POST, ex.getCause());
        }
    }

    public void deletePost(Post post) {
        try {
            postRepository.delete(post);
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_DELETE_POST, ex.getCause());
        }
    }

    public boolean existPostById(Long postId) {
        try {
            return postRepository.existsById(postId);
        } catch (DataAccessException ex) {
            throw new CommonDataBaseException(CANT_CHECK_EXIST_POST, ex.getCause());
        }
    }
}
