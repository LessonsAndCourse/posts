package com.skillbox.posts.service;

import com.skillbox.posts.entity.Post;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Service
public interface PostsMangerService {

    List<Post> getAllUserPosts(Long userId);

    Post createPost(Long userId, Post post, List<MultipartFile> files);

    void savePhotosForPost(Long userId, Long postId,  List<MultipartFile> files);

    void deletePhotoFromPost(Long userId, Long postId, Long photoId);

    void updatePost(Long userId, Long postId, Post post);

    void deleteUserPost(Long userId, Long postId);
}
