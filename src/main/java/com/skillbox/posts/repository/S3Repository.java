package com.skillbox.posts.repository;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.skillbox.posts.exceptions.CommonDataBaseException;
import com.skillbox.posts.property.S3Properties;
import javax.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
@RequiredArgsConstructor
public class S3Repository {

    private static final String CANT_CREATE_BUCKET = "Can't create bucket";
    private static final String CANT_DELETE = "Can't delete file";
    private static final String CANT_PUT = "Can't put file";
    private static final String CANT_GET_FILE_URL = "Can't get file's URL";

    private final S3Properties properties;

    private final AmazonS3 s3Client;

    @PostConstruct
    private void createBucket() {
        try {
            String bucketName = properties.getBucketJpeg();
            if (!s3Client.doesBucketExistV2(bucketName)) {
                s3Client.createBucket(bucketName);
            }
        } catch (AmazonS3Exception ex) {
            throw new CommonDataBaseException(CANT_CREATE_BUCKET, ex.getCause());
        }
    }

    public void delete(String key) {
        try {
            s3Client.deleteObject(properties.getBucketJpeg(), key);
        } catch (AmazonS3Exception ex) {
            throw new CommonDataBaseException(CANT_DELETE, ex.getCause());
        }
    }

    public void put(String key, InputStream inputStream, ObjectMetadata metadata) {
        try {
            s3Client.putObject(properties.getBucketJpeg(), key, inputStream, metadata);
        } catch (AmazonS3Exception ex) {
            throw new CommonDataBaseException(CANT_PUT, ex.getCause());
        }
    }

    public String getFileUrl(String key) {
        try {
            GeneratePresignedUrlRequest urlRequest = new GeneratePresignedUrlRequest(properties.getBucketJpeg(), key);
            return s3Client.generatePresignedUrl(urlRequest).toString();
        } catch (CommonDataBaseException ex) {
            throw new CommonDataBaseException(CANT_GET_FILE_URL, ex.getCause());
        }
    }
}
