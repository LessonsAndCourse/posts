package com.skillbox.posts.dto;

import lombok.Data;

@Data
public class PostDtoRequest {
    private String title;
    private Long userId;
    private String description;
}
