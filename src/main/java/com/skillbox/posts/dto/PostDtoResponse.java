package com.skillbox.posts.dto;

import com.skillbox.posts.entity.Photo;
import lombok.Data;

import java.util.List;

@Data
public class PostDtoResponse {
    private Long id;
    private String title;
    private Long userId;
    private String description;
    private List<Photo> photos;
}

