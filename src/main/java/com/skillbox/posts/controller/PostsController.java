package com.skillbox.posts.controller;

import com.skillbox.posts.converter.PostConverter;
import com.skillbox.posts.dto.PostDtoRequest;
import com.skillbox.posts.dto.PostDtoResponse;
import com.skillbox.posts.entity.Post;
import com.skillbox.posts.service.PostsMangerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/post")
@RequiredArgsConstructor
public class PostsController {

    private final PostsMangerService postsService;
    private final PostConverter postConverter;

    @Operation(summary = "Get all posts for a user")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = Post.class))))})
    @GetMapping("/user/{id}")
    public List<Post> getAllUserPosts(@PathVariable(name = "id") Long userId) {
        return postsService.getAllUserPosts(userId);
    }

    @Operation(summary = "Create a new post")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK",
            content = @Content(schema = @Schema(implementation = Post.class)))})
    @PostMapping(value = "/user/{id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public PostDtoResponse createPost(@PathVariable(name = "id") Long userId,
                                      @ModelAttribute("post") PostDtoRequest postDtoRequest,
                                      @RequestPart("files") List<MultipartFile> files) {
        var postFromDto = postConverter.postDtoRequestToPost(postDtoRequest);
        var savedPost = postsService.createPost(userId, postFromDto, files);
        return postConverter.postToPostResponseDto(savedPost);
    }

    @Operation(summary = "Save images for a post")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK")})
    @PostMapping(value = "/user/{user_id}/post/{post_id}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public void saveImageForPost(@PathVariable(name = "user_id") Long userId,
                                 @PathVariable(name = "post_id") Long postId,
                                 @RequestParam("files") List<MultipartFile> files) {
        postsService.savePhotosForPost(userId, postId, files);
    }

    @Operation(summary = "Delete a photo from a post")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK")})
    @DeleteMapping("/user/{user_id}/post/{post_id}/photo/{photo_id}")
    public void deletePhotoFromPost(@PathVariable(name = "user_id") Long userId,
                                    @PathVariable(name = "post_id") Long postId,
                                    @PathVariable(name = "photo_id") Long photoId) {

        postsService.deletePhotoFromPost(userId, postId, photoId);
    }

    @Operation(summary = "Update a post")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK")})
    @PutMapping("/user/{user_id}/post/{post_id}")
    public void updatePost(@PathVariable(name = "user_id") Long userId,
                           @PathVariable(name = "post_id") Long postId,
                           @RequestBody PostDtoRequest postDtoRequest) {
        var postFromDto = postConverter.postDtoRequestToPost(postDtoRequest);
        postsService.updatePost(userId, postId, postFromDto);
    }

    @Operation(summary = "Delete a user's post")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "OK")})
    @DeleteMapping("/user/{user_id}/post/{post_id}")
    public void deleteUserPost(@PathVariable(name = "user_id") Long userId,
                               @PathVariable(name = "post_id") Long postId) {
        postsService.deleteUserPost(userId, postId);
    }

}
