package com.skillbox.posts.gateway;

import javax.xml.bind.ValidationException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class UsersGateway {

    private final RestTemplate restTemplate;

    public Boolean existUserById(Long userId) {
        var headers = new HttpHeaders();
        var usersUrl = String.format("http://users/user/%d", userId);
        HttpEntity<Map<String, Object>> request = new HttpEntity<>(headers);
        ResponseEntity<String> response = null;
        try {
            response = restTemplate.exchange(
                    usersUrl,
                    HttpMethod.GET,
                    request,
                    String.class
            );
        } catch (Exception e) {
            return false;
        }
        return response.getStatusCode() == HttpStatus.OK;
    }
}
