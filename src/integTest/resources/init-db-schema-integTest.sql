SELECT 'CREATE DATABASE posts' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'posts');

CREATE SCHEMA IF NOT EXISTS posts_scheme;

CREATE TABLE IF NOT EXISTS posts_scheme.posts
(
    post_id SERIAL NOT NULL,
    user_id bigint,
    description character varying(255),
    title character varying(255),
    CONSTRAINT posts_pkey PRIMARY KEY (post_id)
);

CREATE TABLE IF NOT EXISTS posts_scheme.photos
(
    photo_id SERIAL NOT NULL,
    post_post_id bigint,
    link character varying(500),
    name character varying(500),
    CONSTRAINT photos_pkey PRIMARY KEY (photo_id),
    CONSTRAINT fk6qifmldf91107f14rccwwlb7a FOREIGN KEY (post_post_id)
    REFERENCES posts_scheme.posts (post_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    );
