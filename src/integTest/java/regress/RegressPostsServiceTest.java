package regress;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skillbox.posts.PostsApplication;
import com.skillbox.posts.dto.PostDtoRequest;
import com.skillbox.posts.dto.PostDtoResponse;
import com.skillbox.posts.entity.Post;
import com.skillbox.posts.gateway.UsersGateway;
import integ.PostgresContainerWrapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.localstack.LocalStackContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.testcontainers.containers.localstack.LocalStackContainer.Service.S3;

@Testcontainers(disabledWithoutDocker = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(classes = PostsApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class RegressPostsServiceTest {

    @Autowired
    private MockMvc mockMvc;

    private final ObjectMapper objectMapper =  new ObjectMapper();

    @MockBean
    private UsersGateway usersGateway;

    @Container
    private static final PostgreSQLContainer<PostgresContainerWrapper> postgresContainer = new PostgresContainerWrapper();

    @Container
    public static LocalStackContainer localstack = new LocalStackContainer(DockerImageName.parse("localstack/localstack:0.11.3"))
            .withServices(S3);

    AmazonS3 s3 = AmazonS3ClientBuilder
            .standard()
            .withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(
                            localstack.getEndpoint().toString(),
                            localstack.getRegion()
                    )
            )
            .withCredentials(
                    new AWSStaticCredentialsProvider(
                            new BasicAWSCredentials(localstack.getAccessKey(), localstack.getSecretKey())
                    )
            )
            .build();

    @DynamicPropertySource
    public static void initSystemParams(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", postgresContainer::getJdbcUrl);
        registry.add("spring.datasource.username", postgresContainer::getUsername);
        registry.add("spring.datasource.password", postgresContainer::getPassword);

        registry.add("aws.s3.endpoint", localstack::getEndpoint);
        registry.add("aws.s3.access-key", localstack::getAccessKey);
        registry.add("aws.s3.secret-key", localstack::getSecretKey);
        registry.add("aws.s3.region", localstack::getRegion);
    }

    @BeforeEach
    public void init() throws FileNotFoundException {
        String bucketName = "jpeg";
        String filePath1 = "src/integTest/resources/images/image1.png";
        String filePath2 = "src/integTest/resources/images/image2.png";

        when(usersGateway.existUserById(any())).thenReturn(true);

        if (!s3.doesBucketExistV2(bucketName)) {
            s3.createBucket(bucketName);
        }

        File file1 = new File(filePath1);
        File file2 = new File(filePath2);
        ObjectMetadata metadata1 = new ObjectMetadata();
        ObjectMetadata metadata2 = new ObjectMetadata();
        metadata1.setContentLength(file1.length());
        metadata1.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        metadata2.setContentLength(file2.length());
        metadata2.setContentType(String.valueOf(MediaType.IMAGE_JPEG));
        var stream1 = new FileInputStream(file1);
        var stream2 = new FileInputStream(file2);
        s3.putObject(bucketName, file1.getName(), stream1, metadata1);
        s3.putObject(bucketName, file2.getName(), stream2, metadata2);
    }

    @Test
    @Order(1)
    public void create_post_add_photo_get_all_posts() throws Exception {
        Long userId = 100L;
        PostDtoRequest post = new PostDtoRequest();
        post.setUserId(userId);
        post.setDescription("Description");
        post.setTitle("Title");

        String filePath1 = "src/integTest/resources/images/image1.png";
        String filePath2 = "src/integTest/resources/images/image2.png";
        Path file1 = Paths.get(filePath1);
        Path file2 = Paths.get(filePath2);
        // Mock files
        MockMultipartFile fileMock1 = new MockMultipartFile("files", "image1.png", "image/png", Files.readAllBytes(file1));
        MockMultipartFile fileMock2 = new MockMultipartFile("files", "image2.png", "image/png", Files.readAllBytes(file2));

        //when then
        var savedPostString = mockMvc.perform(MockMvcRequestBuilders.multipart("/post/user/{id}", userId)
                        .file(fileMock1)
                        .flashAttr("post", post))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value(post.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.title").value(post.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.photos").isNotEmpty())
                .andReturn().getResponse().getContentAsString();

        var savedPost = objectMapper.readValue(savedPostString, PostDtoResponse.class);

        mockMvc.perform(MockMvcRequestBuilders.multipart("/post/user/{user_id}/post/{post_id}", userId, savedPost.getId())
                        .file(fileMock2))
                .andExpect(MockMvcResultMatchers.status().isOk());

        mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value(post.getDescription()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(post.getTitle()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].userId").value(post.getUserId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].photos.size()").value(2));
    }

    @Test
    @Order(2)
    public void delete_photo_from_post_and_get_all_posts() throws Exception {
        //given
        Long userId = 100L;

        var resultString = mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].photos.size()").value(2))
                .andReturn().getResponse().getContentAsString();

        var result = objectMapper.readValue(resultString, new TypeReference<List<Post>>() {}).get(0);

        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/post/user/{user_id}/post/{post_id}/photo/{photo_id}",
                        userId,
                        result.getId(),
                        result.getPhotos().get(0).getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());

        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].photos.size()").value(1))
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Order(3)
    public void update_post_and_get_all_posts() throws Exception {
        //given
        Long userId = 100L;

        var resultString = mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].photos.size()").value(1))
                .andReturn().getResponse().getContentAsString();

        PostDtoRequest post2 = new PostDtoRequest();
        post2.setUserId(userId);
        post2.setDescription("Description2");
        post2.setTitle("Title2");

        var result = objectMapper.readValue(resultString, new TypeReference<List<Post>>() {}).get(0);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/post/user/{user_id}/post/{post_id}", userId, result.getId(), post2)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(objectMapper.writeValueAsString(post2)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].description").value("Description2"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value("Title2"));
    }

    @Test
    @Order(4)
    public void delete_post_get_all_posts() throws Exception {
        //given
        Long userId = 100L;

        var resultString = mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].photos.size()").value(1))
                .andReturn().getResponse().getContentAsString();

        var result = objectMapper.readValue(resultString, new TypeReference<List<Post>>() {}).get(0);
        //when
        mockMvc.perform(MockMvcRequestBuilders.delete("/post/user/{user_id}/post/{post_id}", userId, result.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk());
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/post/user/{id}", userId))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(0));
    }
}
